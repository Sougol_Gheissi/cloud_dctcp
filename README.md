# Cloud Project

This project is to implement Data Center TCP in the Omnet++ simulator.

## Description

The DCTCP congestion avoidance used in this project is in our other repository, so make sure to have that repository before trying to use this. 

We have implemented two different scenarios to evaluate the performance of DCTCP. 

1. Scenario1: Two long-lived flows destinated to one client to evaluate the Queue Occupancy and the Throughput.

2. Scenario2: Incast phenomenon with forty senders and one receiver to evaluate the Query Completion Time in this case.


## Contributing
Pull requests are welcome. If you are interested in the project please do not hesitate to let us know.


# Members
1. Sougol Gheissi

2. Amrit Nidhi