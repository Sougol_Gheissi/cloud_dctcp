import csv
import numpy as np
import matplotlib.pyplot as plt
queue = [ 4, 6, 8,10]
DC_throughput = []
for q in queue:
    with open('results/DCTCP/DCTCP_pkt_received_'+ str(q) + '.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        first_line = True
        byte_received = 0
        line_count = True
        for row in csv_reader:
            if first_line:
                first_line = False
                continue
            else :
                byte_received += int(row[1])
                end = float(row[0])
                if line_count:
                    start = float(row[0])
                    line_count = False
    th = ((byte_received*8)/(end - start))/1000000000
    DC_throughput.append(th)

throughput = []
with open('results/TCP/TCP_pkt_received.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    first_line = True
    byte_received = 0
    line_count = True
    for row in csv_reader:
        if first_line:
            first_line = False
            continue
        else:
            byte_received += int(row[1])
            end = float(row[0])
            if line_count:
                start = float(row[0])
                line_count = False


th = ((byte_received * 8) / (end - start))/1000000000
for i in range(len(queue)):
    throughput.append(th)
plt.plot(queue,throughput, 'r--', label='TCP')
plt.plot(queue,DC_throughput, 'b',label='DCTCP')
plt.xlabel('K (PKT)')
# naming the y axis
plt.ylabel('Throughput(Gbps)')
plt.legend()
plt.title('TCP vs DCTCP Throughput')
plt.savefig('Figures/Throughput.eps')
plt.show()
