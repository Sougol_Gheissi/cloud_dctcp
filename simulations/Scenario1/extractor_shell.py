import os
from os import listdir
from os.path import isfile, join
from subprocess import call

RESULT_FILES_DIR = './results/'
# OUTPUT_FILE_DIRECTORY = ''

onlyfiles = [f for f in listdir(RESULT_FILES_DIR) if isfile(join(RESULT_FILES_DIR, f)) and f[-4:] == '.vec' in f]
f = open(RESULT_FILES_DIR + 'extractor.sh', 'w')
f.write('mkdir DCTCP\n')
f.write('mkdir TCP\n')
for file_name in onlyfiles:

    print(file_name)
    print("*****************")
    file_main_name = file_name.split('-')[0]
    server_num = file_name.split('-')[1]


    if file_main_name == "DCTCP":
        output_dir_name = 'DCTCP/'
        output_file_name = 'DCTCP_pkt_received_{}.csv'.format(server_num)
        command = "scavetool x --type v --filter \"module(**.client.app[0]) AND " \
              "\\\"packetReceived:vector(packetBytes)\\\"\" -o {} -F CSV-S {}\n".format(
         output_dir_name + output_file_name, file_name)
        print(command)
        f.write(command)
        
        output_file_name = 'DCTCP_queue_{}.csv'.format(server_num)
        command = "scavetool x --type v --filter \"module(**.switch.eth[1].mac.queue) AND " \
              "\\\"queueLength:vector\\\"\" -o {} -F CSV-S {}\n".format(
         output_dir_name + output_file_name, file_name)
        print(command)
        f.write(command)
        
    if file_main_name == "TCP":
        output_dir_name = 'TCP/'
        output_file_name = 'TCP_pkt_received.csv'
        command = "scavetool x --type v --filter \"module(**.client.app[0]) AND " \
              "\\\"packetReceived:vector(packetBytes)\\\"\" -o {} -F CSV-S {}\n".format(
         output_dir_name + output_file_name, file_name)
        print(command)
        f.write(command)
        
        output_file_name = 'TCP_queue.csv'
        command = "scavetool x --type v --filter \"module(**.switch.eth[1].mac.queue) AND " \
              "\\\"queueLength:vector\\\"\" -o {} -F CSV-S {}\n".format(
         output_dir_name + output_file_name, file_name)
        print(command)
        f.write(command)


command = '\n\n'
print(command)
f.write(command)

f.close()
