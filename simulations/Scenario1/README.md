# Cloud Project

This project is to implement Data Center TCP in the Omnet++ simulator.

## Scenario 1

In this directory, we have simulated 2 servers as senders and one client as the receiver. The servers will send data as fast as they can to the client. The client only sends back Acknowledgment. Two different congestion avoidance is used in the transport layer, "TCP" and "DCTCP". The topology in this scenario is shown in the figure below.

![Alt text](./Figures/TOPO.png)


The purpose of this scenario is to evaluate Queue Occupancy and Throughput in both TCP and DCTCP.



## Project Setting
`[Config TCP]

**.tcp.tcpAlgorithmClass = "TcpReno"

**.tcp.ecnWillingness = false

**.switch.eth[*].mac.queue.typename = "DropTailQueue"

**.switch.eth[*].mac.queue.packetCapacity =  20

[Config DCTCP]

**.tcp.tcpAlgorithmClass = "DCTcp"

**.tcp.ecnWillingness = true

**.switch.eth[*].mac.queue.typename = "RedDropperQueue"

**.switch.eth[*].mac.queue.packetCapacity =  20

**.switch.eth[*].mac.queue.red.threshold = ${2, 4, 6, 7, 8, 9, 10, 12, 14, 16} //for throughput

**.switch.eth[*].mac.queue.red.threshold = 4 //for queue occupancy

**.switch.eth[*].mac.queue.red.useEcn = true`


## Results

We find that both TCP and DCTCP achieve high throughput near the maximum throughput of 0.95Gbps, and the link utilization is nearly 100%. In our results, DCTCP got better throughput. The key difference is queue length at the receiver interface.

![picture](./Figures/Throughput.png)

DCTCP operates in a stable manner with low queue occupancy, while TCP fluctuates over time. During the transfer, we sample the instantaneous queue length at the receiver�s switch port every 50ms.

![Alt text](./Figures/Queue.png)

