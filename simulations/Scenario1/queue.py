import csv
import numpy as np
import matplotlib.pyplot as plt

with open('results/DCTCP/DCTCP_queue_4.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    first_line = True
    DC_y = []
    DC_x = []
    for row in csv_reader:
        if first_line:
            first_line = False
            continue
        if float(row[0]) >= 1.29 and float(row[0]) <= 1.82:
            DC_x.append(float(row[0]))
            DC_y.append(float(row[1]))
DC_x_proc = []
DC_y_proc = []
tic = DC_x[0]
print(DC_y[0])
for index,i in enumerate(DC_x):
    if i >= tic:
        if DC_y[ index] != 4:
         	tic = i +0.005
         	continue
        DC_x_proc.append(i)
        DC_y_proc.append(DC_y[index])
        tic = i + 0.005     

with open('results/TCP/TCP_queue.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    first_line = True
    y = []
    x = []
    for row in csv_reader:
        if first_line:
            first_line = False
            continue
        if float(row[0]) >= 1.31 and float(row[0]) <= 1.8:
            x.append(float(row[0]))
            y.append(float(row[1]))
x_proc = []
y_proc = []
tic = y[0]
for index,i in enumerate(x):
    if index % 1024 == 0:
        # temp = i +51
        x_proc.append(i)
        if y[index] > 20:
            y_proc.append(20)
        else:
            y_proc.append(y[index])


plt.plot(x_proc,y_proc, 'r--', label='TCP')
plt.plot(DC_x_proc,DC_y_proc, 'b', label='DCTCP')
plt.legend()
plt.xlabel('Time (s)')
# naming the y axis
plt.ylabel('Queue Capacity(pkt)')



plt.title('TCP vs DCTCP Queue occupancy')
plt.savefig('Figures/Queue.eps')
plt.show()