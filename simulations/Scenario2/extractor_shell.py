import os
from os import listdir
from os.path import isfile, join
from subprocess import call

RESULT_FILES_DIR = './results/'
# OUTPUT_FILE_DIRECTORY = ''

onlyfiles = [f for f in listdir(RESULT_FILES_DIR) if isfile(join(RESULT_FILES_DIR, f)) and f[-4:] == '.vec' in f]
f = open(RESULT_FILES_DIR + 'extractor.sh', 'w')
f.write('mkdir DCTCP\n')
f.write('mkdir TCP\n')
for file_name in onlyfiles:

    print(file_name)
    print("*****************")
    file_main_name = file_name.split('-')[0]
    server_num = file_name.split('=')[1].split('-')[0]

    if file_main_name == "DCTCP":
        output_dir_name = 'DCTCP/'
        output_file_name = 'flowEnd_{}.csv'.format(server_num)
        command = "scavetool x --type v --filter \"module(**.client.app[*]) AND " \
              "\\\"flowEndTime:vector\\\"\" -o {} -F CSV-S {}\n".format(
         output_dir_name + output_file_name, file_name)
        print(command)
        f.write(command)
        
#         output_dir_name = 'DCTCP/'
        output_file_name = 'flowStart_{}.csv'.format(server_num)
        command = "scavetool x --type v --filter \"module(**.client.app[*]) AND " \
              "\\\"flowStartTime:vector\\\"\" -o {} -F CSV-S {}\n".format(
         output_dir_name + output_file_name, file_name)
        print(command)
        f.write(command)
        
    if file_main_name == "TCP":
        output_dir_name = 'TCP/'
        output_file_name = 'flowEnd_{}.csv'.format(server_num)
        command = "scavetool x --type v --filter \"module(**.client.app[*]) AND " \
              "\\\"flowEndTime:vector\\\"\" -o {} -F CSV-S {}\n".format(
         output_dir_name + output_file_name, file_name)
        print(command)
        f.write(command)
        
        output_file_name = 'flowStart_{}.csv'.format(server_num)
        command = "scavetool x --type v --filter \"module(**.client.app[*]) AND " \
              "\\\"flowStartTime:vector\\\"\" -o {} -F CSV-S {}\n".format(
         output_dir_name + output_file_name, file_name)
        print(command)
        f.write(command)

command = '\n\n'
print(command)
f.write(command)

f.close()
