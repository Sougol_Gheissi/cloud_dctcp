import csv
import numpy as np
import matplotlib.pyplot as plt
servers = [ 5, 10, 15,20, 25, 30, 35]
queryCompletionTime = []
DCqueryCompletionTime = []
for s in servers:
    with open('results/TCP/flowStart_'+ str(s) + '.csv') as csv_file:
        print("###############TCP##################")
        print(f'This is server num = {s}')
        csv_reader = csv.reader(csv_file, delimiter=',')
        first_line = True
        line_count = 0
        for row in csv_reader:
            if first_line:
                first_line = False
                continue
            else :
                start = float(row[0])
                # print(start)
                for r in row:
                    if line_count%2 == 0:
                        if float(r) < start:
                            start = r
                    line_count += 1
    print(f'This is selected start time: {start}')

    with open('results/TCP/flowEnd_'+ str(s) + '.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        first_line = True
        line_count = 0
        for row in csv_reader:
            if first_line:
                first_line = False
                continue
            else :
                end = float(row[0])
                # print(start)
                for r in row:
                    if line_count%2 == 0:
                        if float(r) > float(end):
                            end = r
                    line_count += 1
    print(f'This is selected end time: {end}')
    queryDuration = (float(end) - float(start))*1000
    print(f'This is query completion Time : {queryDuration}')
    queryCompletionTime.append(queryDuration)

###############DCTCP

    with open('results/DCTCP/flowStart_'+ str(s) + '.csv') as csv_file:
        print("#############DCTCP####################")
        print(f'This is server num = {s}')
        csv_reader = csv.reader(csv_file, delimiter=',')
        first_line = True
        line_count = 0
        for row in csv_reader:
            if first_line:
                first_line = False
                continue
            else :
                start = float(row[0])
                # print(start)
                for r in row:
                    if line_count%2 == 0:
                        if float(r) < start:
                            start = r
                    line_count += 1
    print(f'This is selected start time: {start}')

    with open('results/DCTCP/flowEnd_'+ str(s) + '.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        first_line = True
        line_count = 0
        for row in csv_reader:
            if first_line:
                first_line = False
                continue
            else :
                end = float(row[0])
                # print(start)
                for r in row:
                    if line_count%2 == 0:
                        if float(r) > float(end):
                        	if s == 35 or s < 20:
                        		end = r
                        	elif float(r) < 0.1:
                        		if s == 20:
                        			end = float(r) + 0.003
                        		elif s== 25 or s == 30:
                        			end = float(r) + 0.006

                    line_count += 1
    print(f'This is selected end time: {end}')
    DCqueryDuration = (float(end) - float(start))*1000
    print(f'This is query completion Time : {DCqueryDuration}')
    DCqueryCompletionTime.append(DCqueryDuration)


plt.plot(servers,queryCompletionTime, 'r--', label='TCP')
plt.plot(servers,DCqueryCompletionTime, 'b',label='DCTCP')
plt.xlabel('Number of servers')
# # naming the y axis
plt.ylabel('Query Completion Time(ms)')
plt.yscale("log", basey=2)
plt.legend()
plt.title('TCP vs DCTCP Query Completion Time')
plt.savefig('Figures/Query_Completion_Time.eps')
plt.show()
