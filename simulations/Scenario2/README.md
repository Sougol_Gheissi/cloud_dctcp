# Cloud Project

This project is to implement Data Center TCP in the Omnet++ simulator.

## Scenario 1

In this directory, we have simulated the Incast phenomenon. TCP Incast is defined as a pathological behavior of TCP that results in under-utilization of the link capacity in various many-to-one communication patterns. This underutilization is due to the fact that highly bursty traffic of multiple TCP connections overflows the Ethernet switch buffer in a short period of time, causing intense packet losses and thus TCP retransmission and timeout. We have forty servers as senders and one client as the receiver under a rack. The client requests ("queries") 1MB/n bytes from n different servers, and each server responds immediately with the requested amount of data. The client waits until it receives all the responses. Two different congestion avoidance is used in the transport layer, "TCP" and "DCTCP". The topology in this scenario is shown in the figure below.

![Alt text](./Figures/TOPO.png)


The purpose of this scenario is to evaluate Query Completion Time in both TCP and DCTCP.



## Project Setting
`[Config TCP]

**.tcp.tcpAlgorithmClass = "TcpReno"

**.tcp.ecnWillingness = false

**.switch.eth[*].mac.queue.typename = "DropTailQueue"

**.switch.eth[*].mac.queue.packetCapacity =  100

[Config DCTCP]

**.tcp.tcpAlgorithmClass = "DCTcp"

**.tcp.ecnWillingness = true

**.switch.eth[*].mac.queue.typename = "RedDropperQueue"

**.switch.eth[*].mac.queue.packetCapacity =  100

**.switch.eth[*].mac.queue.red.threshold = 20

**.switch.eth[*].mac.queue.red.useEcn = true`


## Results

The minimum query completion time is around 8ms. The incoming link at the receiver is the bottleneck, and it takes 8ms to deliver 1MB of data over a 1Gbps link. DCTCP performs much better than TCP.

![Alt text](./Figures/Query_Completion_Time.png)



